package com.winterparadox.wintrymvp.android.viewImpls;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.winterparadox.wintrymvp.R;
import com.winterparadox.wintrymvp.android.Application;
import com.winterparadox.wintrymvp.core.presenters.LoginPresenter;
import com.winterparadox.wintrymvp.core.views.LoginView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoginView {

    @BindView(R.id.login_progress)
    ProgressBar loginProgress;
    @BindView(R.id.email)
    AutoCompleteTextView email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.email_sign_in_button)
    Button emailSignInButton;
    @BindView(R.id.email_login_form)
    LinearLayout emailLoginForm;
    @BindView(R.id.login_form)
    ScrollView loginForm;

    @Inject
    LoginPresenter presenter;
    @BindView(R.id.tvWelcome)
    TextView tvWelcome;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);

        ((Application) getApplication ()).getAppComponent ().inject (this);

        setContentView (R.layout.activity_login);
        ButterKnife.bind (this);

        presenter.attachView (this);
    }

    @Override
    protected void onDestroy () {
        super.onDestroy ();
        presenter.detachView ();
    }

    @OnEditorAction(R.id.password)
    public boolean submit () {
        login ();
        return false;
    }

    @OnClick(R.id.email_sign_in_button)
    public void login () {
        presenter.login (email.getText ().toString (), password.getText ().toString ());
    }

    @Override
    public void onLoggedIn (String userName) {

        String string = getString (R.string.welcome, userName);
        tvWelcome.setText (string);
        showMessage (string);
    }

    @Override
    public void showProgress () {
        loginProgress.setVisibility (View.VISIBLE);
    }

    @Override
    public void hideProgress () {
        loginProgress.setVisibility (View.GONE);
    }

    @Override
    public void showMessage (String message) {
        Toast.makeText (this, message, Toast.LENGTH_SHORT).show ();
    }

    @Override
    public void showError (String message) {
        Toast.makeText (this, message, Toast.LENGTH_LONG).show ();
    }
}

