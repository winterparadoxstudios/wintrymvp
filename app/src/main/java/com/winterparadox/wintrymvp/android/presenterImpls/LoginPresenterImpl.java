package com.winterparadox.wintrymvp.android.presenterImpls;

import com.winterparadox.wintrymvp.core.model.interactors.LoginInteractor;
import com.winterparadox.wintrymvp.core.model.interactors.StorageInteractor;
import com.winterparadox.wintrymvp.core.presenters.LoginPresenter;

/**
 * Created by Adeel on 10/1/2017.
 */

public class LoginPresenterImpl extends LoginPresenter {

    public final LoginInteractor api;
    public final StorageInteractor storage;

    public LoginPresenterImpl (LoginInteractor api, StorageInteractor storage) {

        this.api = api;
        this.storage = storage;
    }


    @Override
    public void login (String email, String password) {
        if (email.isEmpty () ||  password.isEmpty ()) {
            view.showError ("Invalid fields");
            return;
        }

        view.showProgress ();

        api.login (email, password)

                .subscribe ((user, throwable) -> {
                    if (throwable != null) {
                        throwable.printStackTrace ();
                        if ( view != null ) {
                            view.hideProgress ();
                            view.showError (throwable.getMessage ());
                        }
                    } else {
                        if ( view != null ) {
                            storage.saveCurrentUser (user);
                            view.hideProgress ();
                            view.onLoggedIn (user.name);
                        }
                    }
                });
    }
}
