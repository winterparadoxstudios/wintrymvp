package com.winterparadox.wintrymvp.android.interactorImpls;

import android.content.Context;
import android.content.SharedPreferences;

import com.winterparadox.wintrymvp.core.model.beans.User;
import com.winterparadox.wintrymvp.core.model.interactors.StorageInteractor;

/**
 * Created by Adeel on 10/1/2017.
 */

public class StorageInteractorImpl implements StorageInteractor {

    private final SharedPreferences sharedPreferences;

    public StorageInteractorImpl (Context context) {
        sharedPreferences = context.getSharedPreferences ("user", Context.MODE_PRIVATE);
    }

    @Override
    public void saveCurrentUser (User user) {
        SharedPreferences.Editor edit = sharedPreferences.edit ();
        edit.putString ("userName", user.name);
        edit.putString ("accessToken", user.accessToken);
        edit.apply ();
    }
}
