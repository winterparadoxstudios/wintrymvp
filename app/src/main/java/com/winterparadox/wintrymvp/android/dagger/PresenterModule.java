package com.winterparadox.wintrymvp.android.dagger;

import com.winterparadox.wintrymvp.android.presenterImpls.LoginPresenterImpl;
import com.winterparadox.wintrymvp.core.model.interactors.LoginInteractor;
import com.winterparadox.wintrymvp.core.model.interactors.StorageInteractor;
import com.winterparadox.wintrymvp.core.presenters.LoginPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Adeel on 10/1/2017.
 */

@Module
public class PresenterModule {

    @Provides
    @Singleton
    LoginPresenter provideMainPresenter (LoginInteractor api, StorageInteractor storage) {
        return new LoginPresenterImpl (api, storage);
    }
}
