package com.winterparadox.wintrymvp.android.dagger;
import com.winterparadox.wintrymvp.android.Application;
import com.winterparadox.wintrymvp.android.viewImpls.LoginActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Adeel on 8/3/2017.
 */

@Singleton
@Component(modules = {AppModule.class, InteractorModule.class, PresenterModule.class})
public interface AppComponent {

    void inject (Application target);

    void inject (LoginActivity target);
}
