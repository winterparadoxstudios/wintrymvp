package com.winterparadox.wintrymvp.android;

import com.winterparadox.wintrymvp.android.dagger.AppComponent;
import com.winterparadox.wintrymvp.android.dagger.AppModule;
import com.winterparadox.wintrymvp.android.dagger.DaggerAppComponent;

/**
 * Created by Adeel on 10/1/2017.
 */

public class Application extends android.app.Application {

    private AppComponent appComponent;

    @Override
    public void onCreate () {
        super.onCreate ();

        appComponent = initDagger(this);

        getAppComponent ().inject (this);
    }


    protected AppComponent initDagger(Application application) {
        return DaggerAppComponent.builder()
                .appModule(new AppModule (application))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
