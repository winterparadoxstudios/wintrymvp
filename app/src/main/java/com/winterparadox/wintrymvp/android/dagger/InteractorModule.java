package com.winterparadox.wintrymvp.android.dagger;

import android.content.Context;

import com.winterparadox.wintrymvp.android.interactorImpls.LoginInteractorImpl;
import com.winterparadox.wintrymvp.android.interactorImpls.StorageInteractorImpl;
import com.winterparadox.wintrymvp.core.model.interactors.LoginInteractor;
import com.winterparadox.wintrymvp.core.model.interactors.StorageInteractor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Adeel on 10/1/2017.
 */
@Module
public class InteractorModule {

    @Provides
    @Singleton
    StorageInteractor provideStorageInteractor (Context context) {
        return new StorageInteractorImpl (context);
    }

    @Provides
    @Singleton
    LoginInteractor provideLoginInteractor () {
        return new LoginInteractorImpl ();
    }

}
