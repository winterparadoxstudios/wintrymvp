package com.winterparadox.wintrymvp.android.interactorImpls;

import android.os.SystemClock;
import android.util.Log;

import com.winterparadox.wintrymvp.core.model.beans.User;
import com.winterparadox.wintrymvp.core.model.interactors.LoginInteractor;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Adeel on 10/1/2017.
 */

public class LoginInteractorImpl implements LoginInteractor {

    @Override
    public Single<User> login (String email, String password) {
        Single<User> api = Single.create (e -> {
            if ( email.isEmpty () || password.isEmpty () || email.equals (password) ) {
                e.onError (new RuntimeException ("Invalid credentials"));
                return;
            }

            SystemClock.sleep (2000);

            User t = new User ();
            t.accessToken = "abcdefghijklmnopqrstuvwxvz";
            t.name = email;
            Log.d ("api", "on success");
            e.onSuccess (t);
        });

        return api.subscribeOn (Schedulers.io ())
            .observeOn (AndroidSchedulers.mainThread ());
    }
}
