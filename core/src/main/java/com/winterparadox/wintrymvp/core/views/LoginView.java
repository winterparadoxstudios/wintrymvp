package com.winterparadox.wintrymvp.core.views;

/**
 * Created by Adeel on 10/1/2017.
 */

public interface LoginView extends BaseView {

    void onLoggedIn (String userName);
}
