package com.winterparadox.wintrymvp.core.model.interactors;

import com.winterparadox.wintrymvp.core.model.beans.User;

import io.reactivex.Single;

/**
 * Created by Adeel on 10/1/2017.
 */

public interface LoginInteractor {

    Single<User> login (String email, String password);
}
