package com.winterparadox.wintrymvp.core.model.interactors;

import com.winterparadox.wintrymvp.core.model.beans.User;

/**
 * Created by Adeel on 10/1/2017.
 */

public interface StorageInteractor {

    void saveCurrentUser (User user);
}
