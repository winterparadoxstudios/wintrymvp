package com.winterparadox.wintrymvp.core.presenters;

import com.winterparadox.wintrymvp.core.views.LoginView;

/**
 * Created by Adeel on 10/1/2017.
 */

public abstract class LoginPresenter extends BasePresenter <LoginView> {

    public abstract void login (String email, String password);
}
