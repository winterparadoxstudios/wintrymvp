package com.winterparadox.wintrymvp.core.presenters;


public interface Presenter<V> {

    void attachView (V view);

    void detachView ();

}
